# Quasar App (english-test)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

## Copiar spa into firebase/public

### Customize the configuration
See [Configuring quasar.conf.js](https://v2.quasar.dev/quasar-cli/quasar-conf-js).


### Deploy in firebase
cd firebase
firebase deploy --only hosting:practice-english --project presto-prediction 

##
firebase init
firebase login
firebase login --reauth
firebase projects:list