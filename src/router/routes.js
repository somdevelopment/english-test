
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'irregular-verbs', name:'irregular-verbs', component: () => import('src/pages/IrregularVerbs.vue') },
      { path: 'listening', name:'sentenses', component: () => import('src/pages/listening.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
